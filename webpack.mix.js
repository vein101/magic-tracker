const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.setPublicPath('assets');
// mix.setResourceRoot('/assets');
mix.copyDirectory('node_modules/@fortawesome/fontawesome-free/webfonts', 'assets/webfonts');

mix.js('resources/js/app.js', 'assets/js');
mix.sass('resources/sass/app.scss', 'assets/css');

mix.options({
    //   extractVueStyles: false, // Extract .vue component styling to file, rather than inline.
    processCssUrls: false, // Process/optimize relative stylesheet url()'s. Set to false, if you don't want them touched.
    //   purifyCss: true, // Remove unused CSS selectors.
    //   uglify: {}, // Uglify-specific options. https://webpack.github.io/docs/list-of-plugins.html#uglifyjsplugin
    //   postCss: [] // Post-CSS options: https://github.com/postcss/postcss/blob/master/docs/plugins.md
});